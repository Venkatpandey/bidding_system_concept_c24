package de.check24;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Optional;

public class TestStartHere {

	private BiddingSystemImpl biddingSystem;

	@Rule public final ExpectedException thrown = ExpectedException.none();

	@Before public void startBiddingSystem() {
		biddingSystem = new BiddingSystemImpl();
	}

	@Test public void testIfSystemInsertsNullValues1() throws NullPointerException, OverBudgetException {

		thrown.expect(NullPointerException.class);
		thrown.expectMessage("Bid Can not be null");

		biddingSystem.setBidOnSlotByUser(null, Slot.slotGenerator());

	}

	@Test public void testIfSystemInsertsNullValues2() throws NullPointerException, OverBudgetException {

		thrown.expect(NullPointerException.class);
		thrown.expectMessage("Slot can not be null");

		biddingSystem.setBidOnSlotByUser(new Bid(Bidder.bidderGenerator(100L), 105L), null);

	}

	@Test public void testIfBidderHasExceedsItsBudget() throws OverBudgetException {
		thrown.expect(OverBudgetException.class);
		thrown.expectMessage("Bidder Has exceeds its budget");

		Bidder bidder1 = Bidder.bidderGenerator(50L);
		Bidder bidder2 = Bidder.bidderGenerator(50L);
		Bidder bidder3 = Bidder.bidderGenerator(50L);
		Bidder bidder4 = Bidder.bidderGenerator(50L);
		Bidder bidder5 = Bidder.bidderGenerator(50L);
		Bidder bidder6 = Bidder.bidderGenerator(50L);
		Bidder bidder7 = Bidder.bidderGenerator(50L);
		Bidder bidder8 = Bidder.bidderGenerator(50L);
		Bidder bidder9 = Bidder.bidderGenerator(50L);
		Bidder bidder10 = Bidder.bidderGenerator(50L);

		Bid bid1 = new Bid(bidder1, 15L);
		Bid bid2 = new Bid(bidder2, 25L);
		Bid bid3 = new Bid(bidder1, 35L);
		Bid bid4 = new Bid(bidder3, 45L);
		Bid bid5 = new Bid(bidder2, 5L);
		Bid bid6 = new Bid(bidder4, 65L);
		Bid bid7 = new Bid(bidder5, 35L);
		Bid bid8 = new Bid(bidder6, 105L);
		Bid bid9 = new Bid(bidder7, 185L);
		Bid bid10 = new Bid(bidder8, 14L);
		Bid bid11 = new Bid(bidder9, 13L);
		Bid bid12 = new Bid(bidder8, 19L);
		Bid bid13 = new Bid(bidder9, 34L);

		Slot slot1 = Slot.slotGenerator();
		Slot slot2 = Slot.slotGenerator();
		Slot slot3 = Slot.slotGenerator();
		Slot slot4 = Slot.slotGenerator();

		biddingSystem.setBidOnSlotByUser(bid1, slot4);
		biddingSystem.setBidOnSlotByUser(bid2, slot3);
		biddingSystem.setBidOnSlotByUser(bid3, slot2);
		biddingSystem.setBidOnSlotByUser(bid4, slot1);
		biddingSystem.setBidOnSlotByUser(bid5, slot1);
		biddingSystem.setBidOnSlotByUser(bid6, slot2);
		biddingSystem.setBidOnSlotByUser(bid7, slot3);
		biddingSystem.setBidOnSlotByUser(bid8, slot4);
		biddingSystem.setBidOnSlotByUser(bid9, slot4);
		biddingSystem.setBidOnSlotByUser(bid10, slot3);
		biddingSystem.setBidOnSlotByUser(bid11, slot2);
		biddingSystem.setBidOnSlotByUser(bid12, slot2);
		biddingSystem.setBidOnSlotByUser(bid13, slot1);

		final Optional<Bid> highestBidForASlot1 = biddingSystem.getHighestBidForASlot(slot1);
		final Optional<Bid> highestBidForASlot2 = biddingSystem.getHighestBidForASlot(slot2);
		final Optional<Bid> highestBidForASlot3 = biddingSystem.getHighestBidForASlot(slot3);
		final Optional<Bid> highestBidForASlot4 = biddingSystem.getHighestBidForASlot(slot4);

		System.out.println(highestBidForASlot1.toString());
		System.out.println(highestBidForASlot2.toString());
		System.out.println(highestBidForASlot3.toString());
		System.out.println(highestBidForASlot4.toString());
	}

	@Test public void testToFindHeighestBidAndBidder() throws OverBudgetException {
		Long expectedValue1 = 45L;
		Long expectedValue2 = 65L;
		Long expectedValue3 = 35L;
		Long expectedValue4 = 185L;

		Bidder bidder1 = Bidder.bidderGenerator(220L);
		Bidder bidder2 = Bidder.bidderGenerator(220L);
		Bidder bidder3 = Bidder.bidderGenerator(220L);
		Bidder bidder4 = Bidder.bidderGenerator(220L);
		Bidder bidder5 = Bidder.bidderGenerator(220L);
		Bidder bidder6 = Bidder.bidderGenerator(220L);
		Bidder bidder7 = Bidder.bidderGenerator(220L);
		Bidder bidder8 = Bidder.bidderGenerator(220L);
		Bidder bidder9 = Bidder.bidderGenerator(220L);
		Bidder bidder10 = Bidder.bidderGenerator(220L);

		Bid bid1 = new Bid(bidder1, 15L);
		Bid bid2 = new Bid(bidder2, 25L);
		Bid bid3 = new Bid(bidder1, 35L);
		Bid bid4 = new Bid(bidder3, 45L);
		Bid bid5 = new Bid(bidder2, 5L);
		Bid bid6 = new Bid(bidder4, 65L);
		Bid bid7 = new Bid(bidder5, 35L);
		Bid bid8 = new Bid(bidder6, 105L);
		Bid bid9 = new Bid(bidder7, 185L);
		Bid bid10 = new Bid(bidder8, 14L);
		Bid bid11 = new Bid(bidder9, 13L);
		Bid bid12 = new Bid(bidder8, 19L);
		Bid bid13 = new Bid(bidder9, 34L);

		Slot slot1 = Slot.slotGenerator();
		Slot slot2 = Slot.slotGenerator();
		Slot slot3 = Slot.slotGenerator();
		Slot slot4 = Slot.slotGenerator();

		biddingSystem.setBidOnSlotByUser(bid1, slot4);
		biddingSystem.setBidOnSlotByUser(bid2, slot3);
		biddingSystem.setBidOnSlotByUser(bid3, slot2);
		biddingSystem.setBidOnSlotByUser(bid4, slot1);
		biddingSystem.setBidOnSlotByUser(bid5, slot1);
		biddingSystem.setBidOnSlotByUser(bid6, slot2);
		biddingSystem.setBidOnSlotByUser(bid7, slot3);
		biddingSystem.setBidOnSlotByUser(bid8, slot4);
		biddingSystem.setBidOnSlotByUser(bid9, slot4);
		biddingSystem.setBidOnSlotByUser(bid10, slot3);
		biddingSystem.setBidOnSlotByUser(bid11, slot2);
		biddingSystem.setBidOnSlotByUser(bid12, slot2);
		biddingSystem.setBidOnSlotByUser(bid13, slot1);

		final Optional<Bid> highestBidForASlot1 = biddingSystem.getHighestBidForASlot(slot1);
		final Optional<Bid> highestBidForASlot2 = biddingSystem.getHighestBidForASlot(slot2);
		final Optional<Bid> highestBidForASlot3 = biddingSystem.getHighestBidForASlot(slot3);
		final Optional<Bid> highestBidForASlot4 = biddingSystem.getHighestBidForASlot(slot4);

		System.out.println(highestBidForASlot1.toString());
		System.out.println(highestBidForASlot2.toString());
		System.out.println(highestBidForASlot3.toString());
		System.out.println(highestBidForASlot4.toString());

		Assert.assertEquals(expectedValue1, highestBidForASlot1.get().getValue());
		Assert.assertEquals(expectedValue2, highestBidForASlot2.get().getValue());
		Assert.assertEquals(expectedValue3, highestBidForASlot3.get().getValue());
		Assert.assertEquals(expectedValue4, highestBidForASlot4.get().getValue());
	}

}
