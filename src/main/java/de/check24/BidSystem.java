package de.check24;

import java.util.List;
import java.util.Optional;

public interface BidSystem {

	void setBidOnSlotByUser(Bid bid, Slot slot) throws OverBudgetException;

	Optional<Bid> getHighestBidForASlot(Slot slot);

	List<Bid> getCurrentBidsForASlot(Slot slot);

	public Long getTotalBidValueOfBidder(Bidder bidder);

	boolean checkIfBidderExceedsBudget(Bid bid);
}
