package de.check24;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter public class Bid {
	private final Bidder bidder;
	private final Long value;

	public Bid(Bidder bidder, Long value) {
		this.bidder = bidder;
		this.value = value;
	}

	@Override public String toString() {
		return "Bidder = " + this.getBidder().getName() + " Value = " + this.getValue();
	}

	public boolean isBelongTo(Bidder bidder) {
		return this.bidder.equals(bidder);
	}
}
