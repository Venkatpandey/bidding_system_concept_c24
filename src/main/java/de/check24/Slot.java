package de.check24;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Slot {
	private final Long id;
	private final String name;

	public Slot(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public static Slot slotGenerator() {

		return new Slot(Util.getRandonId(), Util.generateRandomName());
	}
}
