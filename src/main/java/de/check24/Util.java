package de.check24;

import java.util.UUID;

public class Util {

	public static Long getRandonId() {

		return (long) Math.random();
	}

	public static String generateRandomName() {

		return UUID.randomUUID().toString();
	}
}
