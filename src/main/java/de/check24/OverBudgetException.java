package de.check24;

public class OverBudgetException extends Exception {

	public OverBudgetException(String msg) {
		super(msg);
	}
}
