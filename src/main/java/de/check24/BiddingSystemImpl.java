package de.check24;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class BiddingSystemImpl implements BidSystem {

	private Map<Slot, List<Bid>> currentPositionBoard;

	public BiddingSystemImpl() {
		this.currentPositionBoard = new ConcurrentHashMap<>();
	}

	public Map<Slot, List<Bid>> getCurrentPositionBoard() {
		return currentPositionBoard;
	}

	@Override public synchronized void setBidOnSlotByUser(Bid bid, Slot slot) throws OverBudgetException {
		Objects.requireNonNull(bid, "Bid Can not be null");

		if (checkIfBidderExceedsBudget(bid)) {
			throw new OverBudgetException("Bidder Has exceeds its budget");
		}

		getCurrentPositionBoard().computeIfAbsent(Objects.requireNonNull(slot, "Slot can not be null"), x -> new ArrayList<>()).add(bid);
	}

	@Override public Optional<Bid> getHighestBidForASlot(Slot slot) {
		List<Bid> currentBidsForASlot = getCurrentBidsForASlot(slot);

		return Optional.of(currentBidsForASlot.stream().max(Comparator.comparing(Bid::getValue)).get());
	}

	@Override public List<Bid> getCurrentBidsForASlot(Slot slot) {

		return getCurrentPositionBoard().getOrDefault(Objects.requireNonNull(slot, "Slot can not be null"), new ArrayList<>());
	}

	@Override public Long getTotalBidValueOfBidder(Bidder bidder) {
		int totalValue = 0;
		List<Bid> bidList = getBidsForThisBidder(bidder);
		for (Bid bid : bidList) {
			totalValue += bid.getValue();
		}

		return (long) totalValue;
	}

	private List<Bid> getBidsForThisBidder(Bidder bidder) {
		List<Bid> bidList = new ArrayList<>();
		getCurrentPositionBoard().entrySet().stream().filter(x -> containsBidder(x.getValue(), bidder))
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).values().forEach(bidList::addAll);

		return bidList;
	}

	private boolean containsBidder(List<Bid> bids, Bidder bidder) {

		return bids.stream().anyMatch(bid -> bid.isBelongTo(bidder));
	}

	@Override public boolean checkIfBidderExceedsBudget(Bid bid) {
		final Bidder bidder = bid.getBidder();
		final Long totalBidValueOfBidder = getTotalBidValueOfBidder(bidder);

		return bidder.getBudget() <= totalBidValueOfBidder;
	}

}
