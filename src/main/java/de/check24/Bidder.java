package de.check24;

import lombok.Getter;
import lombok.Setter;


@Getter @Setter
public class Bidder {
	private final Long id;
	private final String name;
	private final Long budget;

	public Bidder(Long id, String name, Long budget) {
		this.id = id;
		this.name = name;
		this.budget = budget;
	}

	public static Bidder bidderGenerator(Long budget) {

		return new Bidder(Util.getRandonId(), Util.generateRandomName(), budget);
	}
}
