package de.check24;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.StringTokenizer;

public class StartHere {
	public static void main(String[] args) throws OverBudgetException {

		StartHere startHere = new StartHere();

		//final String testCoders = startHere.addLinebreaks("To crop or not to crop", 21);

		int A = 6, B = 1, C = 1;
		generateString(A, B, C);
		//System.out.println(testCoders);
		//startHere.startBiddingSystem();
	}

	static void generateString(int A, int B, int C) {
		String rt = "";
		while (0 < A || 0 < B || 0 < C) {
			// if more b then append like bba
			if (A < B) {
				if (0 < B) {
					rt += ('b');
					B -= 1;
				}
				if (0 < B) {
					rt += ('b');
					B -= 1;
				}
				if (0 < A) {
					rt += ('a');
					A -= 1;
				}
			}

			// if more a the append aab
			else if (B < A) {
				if (0 < A) {
					rt += ('a');
					A -= 1;
				}
				if (0 < A) {
					rt += ('a');
					A -= 1;
				}
				if (0 < B) {
					rt += ('b');
					B -= 1;
				}
			}

			// more c then ccb
			else if (B < C) {
				if (0 < C) {
					rt += ('C');
					C -= 1;
				}
				if (0 < C) {
					rt += ('C');
					C -= 1;
				}

				if (0 < B) {
					rt += ('b');
					B -= 1;
				}
			}

			// more a then aac
			else if (C < A) {
				if (0 < A) {
					rt += ('a');
					A -= 1;
				}
				if (0 < A) {
					rt += ('a');
					A -= 1;
				}
				if (0 < C) {
					rt += ('c');
					C -= 1;
				}
			}

			// more a then cca
			else if (A < C) {
				if (0 < C) {
					rt += ('c');
					C -= 1;
				}
				if (0 < C) {
					rt += ('c');
					C -= 1;
				}
				if (0 < A) {
					rt += ('a');
					A -= 1;
				}
			}

			//more b the bbc
			else if (C < B) {
				if (0 < B) {
					rt += ('b');
					B -= 1;
				}
				if (0 < B) {
					rt += ('b');
					B -= 1;
				}
				if (0 < C) {
					rt += ('c');
					C -= 1;
				}
			}

			// for equal append abc

			else {

				if (0 < A) {
					rt += ('a');
					A -= 1;
				}

				if (0 < B) {
					rt += ('b');
					B -= 1;
				}

				if (0 < C) {
					rt += ('c');
					C -= 1;
				}
			}

		}
		System.out.println(rt);
	}

	public String addLinebreaks(String input, int maxLineLength) {
		StringTokenizer tok = new StringTokenizer(input, " ");
		StringBuilder output = new StringBuilder(input.length());
		int lineLen = 0;
		while (tok.hasMoreTokens()) {
			String word = tok.nextToken();
			if (!(lineLen + word.length() > maxLineLength)) {
				output.append(word).append(" ");
			}
			lineLen += word.length() + 1;
		}

		return output.toString().trim();
	}

	private void startBiddingSystem() throws OverBudgetException {
		BiddingSystemImpl biddingSystem = new BiddingSystemImpl();

		Bidder bidder1 = Bidder.bidderGenerator(220L);
		Bidder bidder2 = Bidder.bidderGenerator(220L);
		Bidder bidder3 = Bidder.bidderGenerator(220L);
		Bidder bidder4 = Bidder.bidderGenerator(220L);
		Bidder bidder5 = Bidder.bidderGenerator(220L);
		Bidder bidder6 = Bidder.bidderGenerator(220L);
		Bidder bidder7 = Bidder.bidderGenerator(220L);
		Bidder bidder8 = Bidder.bidderGenerator(220L);
		Bidder bidder9 = Bidder.bidderGenerator(220L);
		Bidder bidder10 = Bidder.bidderGenerator(220L);

		Bid bid1 = new Bid(bidder1, 15L);
		Bid bid2 = new Bid(bidder2, 25L);
		Bid bid3 = new Bid(bidder1, 35L);
		Bid bid4 = new Bid(bidder3, 45L);
		Bid bid5 = new Bid(bidder2, 5L);
		Bid bid6 = new Bid(bidder4, 65L);
		Bid bid7 = new Bid(bidder5, 35L);
		Bid bid8 = new Bid(bidder6, 105L);
		Bid bid9 = new Bid(bidder7, 185L);
		Bid bid10 = new Bid(bidder8, 14L);
		Bid bid11 = new Bid(bidder9, 13L);
		Bid bid12 = new Bid(bidder8, 19L);
		Bid bid13 = new Bid(bidder9, 34L);

		Slot slot1 = Slot.slotGenerator();
		Slot slot2 = Slot.slotGenerator();
		Slot slot3 = Slot.slotGenerator();
		Slot slot4 = Slot.slotGenerator();

		biddingSystem.setBidOnSlotByUser(bid1, slot4);
		biddingSystem.setBidOnSlotByUser(bid2, slot3);
		biddingSystem.setBidOnSlotByUser(bid3, slot2);
		biddingSystem.setBidOnSlotByUser(bid4, slot1);
		biddingSystem.setBidOnSlotByUser(bid5, slot1);
		biddingSystem.setBidOnSlotByUser(bid6, slot2);
		biddingSystem.setBidOnSlotByUser(bid7, slot3);
		biddingSystem.setBidOnSlotByUser(bid8, slot4);
		biddingSystem.setBidOnSlotByUser(bid9, slot4);
		biddingSystem.setBidOnSlotByUser(bid10, slot3);
		biddingSystem.setBidOnSlotByUser(bid11, slot2);
		biddingSystem.setBidOnSlotByUser(bid12, slot2);
		biddingSystem.setBidOnSlotByUser(bid13, slot1);

		final Optional<Bid> highestBidForASlot1 = biddingSystem.getHighestBidForASlot(slot1);
		final Optional<Bid> highestBidForASlot2 = biddingSystem.getHighestBidForASlot(slot2);
		final Optional<Bid> highestBidForASlot3 = biddingSystem.getHighestBidForASlot(slot3);
		final Optional<Bid> highestBidForASlot4 = biddingSystem.getHighestBidForASlot(slot4);

		System.out.println(highestBidForASlot1.toString());
		System.out.println(highestBidForASlot2.toString());
		System.out.println(highestBidForASlot3.toString());
		System.out.println(highestBidForASlot4.toString());
	}
}
